//
// Created by Apopis on 25.12.2017.
//

#ifndef GRAPHAISDI_EDGEHASH_H
#define GRAPHAISDI_EDGEHASH_H

#include <functional>

namespace std
{
    template<> struct hash<pair<size_t,size_t>>
    {
        typedef pair<size_t,size_t> argument_type;

        size_t operator()(argument_type const& arg) const noexcept
        {
            size_t const h1(hash<size_t>{}(arg.first));
            size_t const h2(hash<size_t>{}(arg.second));
            return h1 ^ (h2 << 1);
        }
    };
}


#endif //GRAPHAISDI_EDGEHASH_H
