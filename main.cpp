#include <iostream>
#include "BridgeFinder.h"

using namespace std;

int main() {
    size_t n;
    cin >> n;
    Graph graph(n);

    size_t vertex1;
    size_t vertex2;

    while (cin >> vertex1 >> vertex2)
        graph.addEdge(vertex1,vertex2);

    BridgeFinder bf(graph);
    vector<pair<size_t,size_t>> result = bf.findExtBridges();

    for (const auto &r : result)
        cout << r.first << " " << r.second << endl;

    return 0;
}