//
// Created by Apopis on 25.12.2017.
//

#ifndef GRAPHAISDI_GRAPH_H
#define GRAPHAISDI_GRAPH_H

#include <unordered_set>
#include <vector>
#include "EdgeHash.h"

class Graph {
    class Vertex {
        std::unordered_set<size_t> adjacent;
    public:
        void addAdjacent(const size_t&);
        const std::unordered_set<size_t>& getAdjacent() const;
    };

    using Edge = std::pair<size_t, size_t>;

    std::vector<Vertex> vertices;
    std::unordered_set<Edge> edges;

public:
    Graph() = default;
    explicit Graph(const size_t&);
    Graph(const Graph&) = default;
    Graph(Graph&&) = default;

    Graph& operator=(const Graph&) = default;
    Graph& operator=(Graph&&) = default;

    void addNode();
    void addEdge(const size_t&, const size_t&);
    const std::unordered_set<Edge>& getEdges() const;
    const Vertex& getVertex(const size_t&) const;
    size_t getNoVertices() const;
};

#endif //GRAPHAISDI_GRAPH_H
