//
// Created by Apopis on 25.12.2017.
//

#ifndef GRAPHAISDI_BRIDGEFINDER_H
#define GRAPHAISDI_BRIDGEFINDER_H

#include "Graph.h"

class BridgeFinder {
    using Edge = std::pair<size_t,size_t>;

    const Graph *graph;

    void bfs(const size_t&,std::unordered_set<size_t>&) const;
    size_t bfsStart(const size_t&, const size_t&) const;
public:
    BridgeFinder() = default;
    explicit BridgeFinder(const Graph&);
    BridgeFinder(const BridgeFinder&) = default;
    BridgeFinder(BridgeFinder&&) = default;

    BridgeFinder& operator=(const BridgeFinder&) = default;
    BridgeFinder& operator=(BridgeFinder&&) = default;

    std::vector<Edge> findExtBridges() const;
};

#endif //GRAPHAISDI_BRIDGEFINDER_H
