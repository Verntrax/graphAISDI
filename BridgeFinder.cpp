//
// Created by Apopis on 25.12.2017.
//

#include <queue>
#include "BridgeFinder.h"

using namespace std;

BridgeFinder::BridgeFinder(const Graph &graph)
        : graph(&graph)
{}

void BridgeFinder::bfs(const size_t &startVertex, unordered_set<size_t> &container) const {
    queue<size_t> st;
    st.push(startVertex);

    while (!st.empty()) {
        const size_t &vertex = st.front();
        st.pop();
        const unordered_set<size_t> &adjacent = graph->getVertex(vertex).getAdjacent();

        for (const size_t &next : adjacent) {
            if (container.count(next) == 0)
                st.push(next);
        }
        container.insert(vertex);
    }
}

size_t BridgeFinder::bfsStart(const size_t &first, const size_t &second) const {
    const size_t n = graph->getNoVertices();
    size_t result = (first + 1) % n;
    if (result == second)
        result = (result + 1) % n;
    return result;
}

vector<BridgeFinder::Edge> BridgeFinder::findExtBridges() const {
    vector<Edge> result;
    const size_t n = graph->getNoVertices();

    if (n > 2) {
        for (const Edge &edge : graph->getEdges()) {
            unordered_set<size_t> vertexSet;
            vertexSet.insert(edge.first);
            vertexSet.insert(edge.second);

            size_t startVertex = bfsStart(edge.first,edge.second);
            bfs(startVertex,vertexSet);

            if (vertexSet.size() != n)
                result.push_back(edge);
        }
    }
    return result;
}