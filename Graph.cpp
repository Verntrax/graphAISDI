//
// Created by Apopis on 25.12.2017.
//

#include <algorithm>
#include "Graph.h"

using namespace std;

void Graph::Vertex::addAdjacent(const size_t& other) {
    adjacent.insert(other);
}

const unordered_set<size_t>& Graph::Vertex::getAdjacent() const {
    return adjacent;
}

Graph::Graph(const size_t &size)
        : vertices(size), edges()
{}

void Graph::addNode() {
    vertices.emplace_back();
}

void Graph::addEdge(const size_t &firstNode, const size_t &secondNode) {
    Edge edge = minmax(firstNode, secondNode);
    edges.insert(edge);
    vertices[firstNode].addAdjacent(secondNode);
    vertices[secondNode].addAdjacent(firstNode);
}

const unordered_set<Graph::Edge>& Graph::getEdges() const {
    return edges;
}

const Graph::Vertex& Graph::getVertex(const size_t &index) const {
    return vertices[index];
}

size_t Graph::getNoVertices() const {
    return vertices.size();
}